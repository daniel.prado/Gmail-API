Para configurar a aplicação para utilizar a API do Gmail é necessários realizar as configurações abaixo.

- Acessar o console da API do Google da conta que deseja utilizar
    https://console.developers.google.com/

- Criar um novo projeto
- Criar nova credencial
    .IDs do cliente OAuth 2.0.
    .Fazer o download do json com as credenciais:
        .exemplo:
        {
          "installed": {
        
            "client_id": "8279405asdfasxxxxxxxxxxxxxxiahdmhqu.apps.googleusercontent.com",
            "project_id": "orbapie-mail",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_secret": "fasdfaefawefa-afsdfawe",
            "redirect_uris": [ "urn:ietf:wg:oauth:2.0:oob", "http://localhost" ]
      }
    }
    
- Add o arquivo json acima a raiz do projeto
- Ir as propriedades do arquivo e alterar "Copy to Output Directo" para "Copy always"

- Instalar os pacotes nugets abaixo:

    Install-Package Google.Apis -Version 1.43.0
    Install-Package Google.Apis.Gmail.v1 -Version 1.43.0.1859
    Install-Package MimeKit -Version 2.5.1

