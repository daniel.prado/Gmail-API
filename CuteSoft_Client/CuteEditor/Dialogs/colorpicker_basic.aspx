<%@ Page Language="C#" Inherits="CuteEditor.EditorUtilityPage" %>
<script runat="server">
string GetDialogQueryString;
override protected void OnInit(EventArgs args)
{
	if(Context.Request.QueryString["Dialog"]=="Standard")
	{	
	if(Context.Request.QueryString["IsFrame"]==null)
	{
		string FrameSrc="colorpicker_basic.aspx?IsFrame=1&"+Request.ServerVariables["QUERY_STRING"];
		CuteEditor.CEU.WriteDialogOuterFrame(Context,"[[MoreColors]]",FrameSrc);
		Context.Response.End();
	}
	}
	string s="";
	if(Context.Request.QueryString["Dialog"]=="Standard")	
		s="&Dialog=Standard";
	
	GetDialogQueryString="Theme="+Context.Request.QueryString["Theme"]+s;	
	base.OnInit(args);
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.1)" />
		<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.1)" />
		<script type="text/javascript" src="Load.ashx?type=dialogscript&verfix=1006&file=DialogHead.js"></script>
		<script type="text/javascript" src="Load.ashx?type=dialogscript&verfix=1006&file=Dialog_ColorPicker.js"></script>
		<link href='Load.ashx?type=themecss&file=dialog.css&theme=[[_Theme_]]' type="text/css"
			rel="stylesheet" />
		<style type="text/css">
			.colorcell
			{
				width:16px;
				height:17px;
				cursor:hand;
			}
			.colordiv,.customdiv
			{
				border:solid 1px #808080;
				width:16px;
				height:17px;
				font-size:1px;
			}
			#ajaxdiv{padding:10px;margin:0;text-align:center; background:#eeeeee;}
		</style>
		<title>[[NamedColors]]</title>
		<script>
								
		var OxO8e1e=["Green","#008000","Lime","#00FF00","Teal","#008080","Aqua","#00FFFF","Navy","#000080","Blue","#0000FF","Purple","#800080","Fuchsia","#FF00FF","Maroon","#800000","Red","#FF0000","Olive","#808000","Yellow","#FFFF00","White","#FFFFFF","Silver","#C0C0C0","Gray","#808080","Black","#000000","DarkOliveGreen","#556B2F","DarkGreen","#006400","DarkSlateGray","#2F4F4F","SlateGray","#708090","DarkBlue","#00008B","MidnightBlue","#191970","Indigo","#4B0082","DarkMagenta","#8B008B","Brown","#A52A2A","DarkRed","#8B0000","Sienna","#A0522D","SaddleBrown","#8B4513","DarkGoldenrod","#B8860B","Beige","#F5F5DC","HoneyDew","#F0FFF0","DimGray","#696969","OliveDrab","#6B8E23","ForestGreen","#228B22","DarkCyan","#008B8B","LightSlateGray","#778899","MediumBlue","#0000CD","DarkSlateBlue","#483D8B","DarkViolet","#9400D3","MediumVioletRed","#C71585","IndianRed","#CD5C5C","Firebrick","#B22222","Chocolate","#D2691E","Peru","#CD853F","Goldenrod","#DAA520","LightGoldenrodYellow","#FAFAD2","MintCream","#F5FFFA","DarkGray","#A9A9A9","YellowGreen","#9ACD32","SeaGreen","#2E8B57","CadetBlue","#5F9EA0","SteelBlue","#4682B4","RoyalBlue","#4169E1","BlueViolet","#8A2BE2","DarkOrchid","#9932CC","DeepPink","#FF1493","RosyBrown","#BC8F8F","Crimson","#DC143C","DarkOrange","#FF8C00","BurlyWood","#DEB887","DarkKhaki","#BDB76B","LightYellow","#FFFFE0","Azure","#F0FFFF","LightGray","#D3D3D3","LawnGreen","#7CFC00","MediumSeaGreen","#3CB371","LightSeaGreen","#20B2AA","DeepSkyBlue","#00BFFF","DodgerBlue","#1E90FF","SlateBlue","#6A5ACD","MediumOrchid","#BA55D3","PaleVioletRed","#DB7093","Salmon","#FA8072","OrangeRed","#FF4500","SandyBrown","#F4A460","Tan","#D2B48C","Gold","#FFD700","Ivory","#FFFFF0","GhostWhite","#F8F8FF","Gainsboro","#DCDCDC","Chartreuse","#7FFF00","LimeGreen","#32CD32","MediumAquamarine","#66CDAA","DarkTurquoise","#00CED1","CornflowerBlue","#6495ED","MediumSlateBlue","#7B68EE","Orchid","#DA70D6","HotPink","#FF69B4","LightCoral","#F08080","Tomato","#FF6347","Orange","#FFA500","Bisque","#FFE4C4","Khaki","#F0E68C","Cornsilk","#FFF8DC","Linen","#FAF0E6","WhiteSmoke","#F5F5F5","GreenYellow","#ADFF2F","DarkSeaGreen","#8FBC8B","Turquoise","#40E0D0","MediumTurquoise","#48D1CC","SkyBlue","#87CEEB","MediumPurple","#9370DB","Violet","#EE82EE","LightPink","#FFB6C1","DarkSalmon","#E9967A","Coral","#FF7F50","NavajoWhite","#FFDEAD","BlanchedAlmond","#FFEBCD","PaleGoldenrod","#EEE8AA","Oldlace","#FDF5E6","Seashell","#FFF5EE","PaleGreen","#98FB98","SpringGreen","#00FF7F","Aquamarine","#7FFFD4","PowderBlue","#B0E0E6","LightSkyBlue","#87CEFA","LightSteelBlue","#B0C4DE","Plum","#DDA0DD","Pink","#FFC0CB","LightSalmon","#FFA07A","Wheat","#F5DEB3","Moccasin","#FFE4B5","AntiqueWhite","#FAEBD7","LemonChiffon","#FFFACD","FloralWhite","#FFFAF0","Snow","#FFFAFA","AliceBlue","#F0F8FF","LightGreen","#90EE90","MediumSpringGreen","#00FA9A","PaleTurquoise","#AFEEEE","LightCyan","#E0FFFF","LightBlue","#ADD8E6","Lavender","#E6E6FA","Thistle","#D8BFD8","MistyRose","#FFE4E1","Peachpuff","#FFDAB9","PapayaWhip","#FFEFD5"];var colorlist=[{n:OxO8e1e[0],h:OxO8e1e[1]},{n:OxO8e1e[2],h:OxO8e1e[3]},{n:OxO8e1e[4],h:OxO8e1e[5]},{n:OxO8e1e[6],h:OxO8e1e[7]},{n:OxO8e1e[8],h:OxO8e1e[9]},{n:OxO8e1e[10],h:OxO8e1e[11]},{n:OxO8e1e[12],h:OxO8e1e[13]},{n:OxO8e1e[14],h:OxO8e1e[15]},{n:OxO8e1e[16],h:OxO8e1e[17]},{n:OxO8e1e[18],h:OxO8e1e[19]},{n:OxO8e1e[20],h:OxO8e1e[21]},{n:OxO8e1e[22],h:OxO8e1e[23]},{n:OxO8e1e[24],h:OxO8e1e[25]},{n:OxO8e1e[26],h:OxO8e1e[27]},{n:OxO8e1e[28],h:OxO8e1e[29]},{n:OxO8e1e[30],h:OxO8e1e[31]}];var colormore=[{n:OxO8e1e[32],h:OxO8e1e[33]},{n:OxO8e1e[34],h:OxO8e1e[35]},{n:OxO8e1e[36],h:OxO8e1e[37]},{n:OxO8e1e[38],h:OxO8e1e[39]},{n:OxO8e1e[40],h:OxO8e1e[41]},{n:OxO8e1e[42],h:OxO8e1e[43]},{n:OxO8e1e[44],h:OxO8e1e[45]},{n:OxO8e1e[46],h:OxO8e1e[47]},{n:OxO8e1e[48],h:OxO8e1e[49]},{n:OxO8e1e[50],h:OxO8e1e[51]},{n:OxO8e1e[52],h:OxO8e1e[53]},{n:OxO8e1e[54],h:OxO8e1e[55]},{n:OxO8e1e[56],h:OxO8e1e[57]},{n:OxO8e1e[58],h:OxO8e1e[59]},{n:OxO8e1e[60],h:OxO8e1e[61]},{n:OxO8e1e[62],h:OxO8e1e[63]},{n:OxO8e1e[64],h:OxO8e1e[65]},{n:OxO8e1e[66],h:OxO8e1e[67]},{n:OxO8e1e[68],h:OxO8e1e[69]},{n:OxO8e1e[70],h:OxO8e1e[71]},{n:OxO8e1e[72],h:OxO8e1e[73]},{n:OxO8e1e[74],h:OxO8e1e[75]},{n:OxO8e1e[76],h:OxO8e1e[77]},{n:OxO8e1e[78],h:OxO8e1e[79]},{n:OxO8e1e[80],h:OxO8e1e[81]},{n:OxO8e1e[82],h:OxO8e1e[83]},{n:OxO8e1e[84],h:OxO8e1e[85]},{n:OxO8e1e[86],h:OxO8e1e[87]},{n:OxO8e1e[88],h:OxO8e1e[89]},{n:OxO8e1e[90],h:OxO8e1e[91]},{n:OxO8e1e[92],h:OxO8e1e[93]},{n:OxO8e1e[94],h:OxO8e1e[95]},{n:OxO8e1e[96],h:OxO8e1e[97]},{n:OxO8e1e[98],h:OxO8e1e[99]},{n:OxO8e1e[100],h:OxO8e1e[101]},{n:OxO8e1e[102],h:OxO8e1e[103]},{n:OxO8e1e[104],h:OxO8e1e[105]},{n:OxO8e1e[106],h:OxO8e1e[107]},{n:OxO8e1e[108],h:OxO8e1e[109]},{n:OxO8e1e[110],h:OxO8e1e[111]},{n:OxO8e1e[112],h:OxO8e1e[113]},{n:OxO8e1e[114],h:OxO8e1e[115]},{n:OxO8e1e[116],h:OxO8e1e[117]},{n:OxO8e1e[118],h:OxO8e1e[119]},{n:OxO8e1e[120],h:OxO8e1e[121]},{n:OxO8e1e[122],h:OxO8e1e[123]},{n:OxO8e1e[124],h:OxO8e1e[125]},{n:OxO8e1e[126],h:OxO8e1e[127]},{n:OxO8e1e[128],h:OxO8e1e[129]},{n:OxO8e1e[130],h:OxO8e1e[131]},{n:OxO8e1e[132],h:OxO8e1e[133]},{n:OxO8e1e[134],h:OxO8e1e[135]},{n:OxO8e1e[136],h:OxO8e1e[137]},{n:OxO8e1e[138],h:OxO8e1e[139]},{n:OxO8e1e[140],h:OxO8e1e[141]},{n:OxO8e1e[142],h:OxO8e1e[143]},{n:OxO8e1e[144],h:OxO8e1e[145]},{n:OxO8e1e[146],h:OxO8e1e[147]},{n:OxO8e1e[148],h:OxO8e1e[149]},{n:OxO8e1e[150],h:OxO8e1e[151]},{n:OxO8e1e[152],h:OxO8e1e[153]},{n:OxO8e1e[154],h:OxO8e1e[155]},{n:OxO8e1e[156],h:OxO8e1e[157]},{n:OxO8e1e[158],h:OxO8e1e[159]},{n:OxO8e1e[160],h:OxO8e1e[161]},{n:OxO8e1e[162],h:OxO8e1e[163]},{n:OxO8e1e[164],h:OxO8e1e[165]},{n:OxO8e1e[166],h:OxO8e1e[167]},{n:OxO8e1e[168],h:OxO8e1e[169]},{n:OxO8e1e[170],h:OxO8e1e[171]},{n:OxO8e1e[172],h:OxO8e1e[173]},{n:OxO8e1e[174],h:OxO8e1e[175]},{n:OxO8e1e[176],h:OxO8e1e[177]},{n:OxO8e1e[178],h:OxO8e1e[179]},{n:OxO8e1e[180],h:OxO8e1e[181]},{n:OxO8e1e[182],h:OxO8e1e[183]},{n:OxO8e1e[184],h:OxO8e1e[185]},{n:OxO8e1e[186],h:OxO8e1e[187]},{n:OxO8e1e[188],h:OxO8e1e[189]},{n:OxO8e1e[190],h:OxO8e1e[191]},{n:OxO8e1e[192],h:OxO8e1e[193]},{n:OxO8e1e[194],h:OxO8e1e[195]},{n:OxO8e1e[196],h:OxO8e1e[197]},{n:OxO8e1e[198],h:OxO8e1e[199]},{n:OxO8e1e[200],h:OxO8e1e[201]},{n:OxO8e1e[202],h:OxO8e1e[203]},{n:OxO8e1e[204],h:OxO8e1e[205]},{n:OxO8e1e[206],h:OxO8e1e[207]},{n:OxO8e1e[208],h:OxO8e1e[209]},{n:OxO8e1e[210],h:OxO8e1e[211]},{n:OxO8e1e[212],h:OxO8e1e[213]},{n:OxO8e1e[214],h:OxO8e1e[215]},{n:OxO8e1e[216],h:OxO8e1e[217]},{n:OxO8e1e[218],h:OxO8e1e[219]},{n:OxO8e1e[220],h:OxO8e1e[221]},{n:OxO8e1e[156],h:OxO8e1e[157]},{n:OxO8e1e[222],h:OxO8e1e[223]},{n:OxO8e1e[224],h:OxO8e1e[225]},{n:OxO8e1e[226],h:OxO8e1e[227]},{n:OxO8e1e[228],h:OxO8e1e[229]},{n:OxO8e1e[230],h:OxO8e1e[231]},{n:OxO8e1e[232],h:OxO8e1e[233]},{n:OxO8e1e[234],h:OxO8e1e[235]},{n:OxO8e1e[236],h:OxO8e1e[237]},{n:OxO8e1e[238],h:OxO8e1e[239]},{n:OxO8e1e[240],h:OxO8e1e[241]},{n:OxO8e1e[242],h:OxO8e1e[243]},{n:OxO8e1e[244],h:OxO8e1e[245]},{n:OxO8e1e[246],h:OxO8e1e[247]},{n:OxO8e1e[248],h:OxO8e1e[249]},{n:OxO8e1e[250],h:OxO8e1e[251]},{n:OxO8e1e[252],h:OxO8e1e[253]},{n:OxO8e1e[254],h:OxO8e1e[255]},{n:OxO8e1e[256],h:OxO8e1e[257]},{n:OxO8e1e[258],h:OxO8e1e[259]},{n:OxO8e1e[260],h:OxO8e1e[261]},{n:OxO8e1e[262],h:OxO8e1e[263]},{n:OxO8e1e[264],h:OxO8e1e[265]},{n:OxO8e1e[266],h:OxO8e1e[267]},{n:OxO8e1e[268],h:OxO8e1e[269]},{n:OxO8e1e[270],h:OxO8e1e[271]},{n:OxO8e1e[272],h:OxO8e1e[273]}];
		
		</script>
	</head>
	<body>
		<div id="ajaxdiv">
			<div class="tab-pane-control tab-pane" id="tabPane1">
				<div class="tab-row">
					<h2 class="tab">
						<a tabindex="-1" href='colorpicker.aspx?<%=GetDialogQueryString%>'>
							<span style="white-space:nowrap;">
								[[WebPalette]]
							</span>
						</a>
					</h2>
					<h2 class="tab selected">
							<a tabindex="-1" href='colorpicker_basic.aspx?<%=GetDialogQueryString%>'>
								<span style="white-space:nowrap;">
									[[NamedColors]]
								</span>
							</a>
					</h2>
					<h2 class="tab">
							<a tabindex="-1" href='colorpicker_more.aspx?<%=GetDialogQueryString%>'>
								<span style="white-space:nowrap;">
									[[CustomColor]]
								</span>
							</a>
					</h2>
				</div>
				<div class="tab-page">			
					<table class="colortable" align="center">
						<tr>
							<td colspan="16" height="16"><p align="left">Basic:
								</p>
							</td>
						</tr>
						<tr>
							<script>
								var OxOf99b=["length","\x3Ctd class=\x27colorcell\x27\x3E\x3Cdiv class=\x27colordiv\x27 style=\x27background-color:","\x27 title=\x27"," ","\x27 cname=\x27","\x27 cvalue=\x27","\x27\x3E\x3C/div\x3E\x3C/td\x3E",""];var arr=[];for(var i=0;i<colorlist[OxOf99b[0]];i++){arr.push(OxOf99b[1]);arr.push(colorlist[i].n);arr.push(OxOf99b[2]);arr.push(colorlist[i].n);arr.push(OxOf99b[3]);arr.push(colorlist[i].h);arr.push(OxOf99b[4]);arr.push(colorlist[i].n);arr.push(OxOf99b[5]);arr.push(colorlist[i].h);arr.push(OxOf99b[6]);} ;document.write(arr.join(OxOf99b[7]));
							</script>
						</tr>
						<tr>
							<td colspan="16" height="12"><p align="left"></p>
							</td>
						</tr>
						<tr>
							<td colspan="16"><p align="left">Additional:
								</p>
							</td>
						</tr>
						<script>
							var OxO54f5=["length","\x3Ctr\x3E","\x3Ctd class=\x27colorcell\x27\x3E\x3Cdiv class=\x27colordiv\x27 style=\x27background-color:","\x27 title=\x27"," ","\x27 cname=\x27","\x27 cvalue=\x27","\x27\x3E\x3C/div\x3E\x3C/td\x3E","\x3C/tr\x3E",""];var arr=[];for(var i=0;i<colormore[OxO54f5[0]];i++){if(i%16==0){arr.push(OxO54f5[1]);} ;arr.push(OxO54f5[2]);arr.push(colormore[i].n);arr.push(OxO54f5[3]);arr.push(colormore[i].n);arr.push(OxO54f5[4]);arr.push(colormore[i].h);arr.push(OxO54f5[5]);arr.push(colormore[i].n);arr.push(OxO54f5[6]);arr.push(colormore[i].h);arr.push(OxO54f5[7]);if(i%16==15){arr.push(OxO54f5[8]);} ;} ;if(colormore%16>0){arr.push(OxO54f5[8]);} ;document.write(arr.join(OxO54f5[9]));
						</script>
						<tr>
							<td colspan="16" height="8">
							</td>
						</tr>
						<tr>
							<td colspan="16" height="12">
								<input checked id="CheckboxColorNames" style="width: 16px; height: 20px" type="checkbox">
								<span style="width: 118px;">Use color names</span>
							</td>
						</tr>
						<tr>
							<td colspan="16" height="12">
							</td>
						</tr>
						<tr>
							<td colspan="16" valign="middle" height="24">
							<span style="height:24px;width:50px;vertical-align:middle;">Color : </span>&nbsp;
							<input type="text" id="divpreview" size="7" maxlength="7" style="width:180px;height:24px;border:#a0a0a0 1px solid; Padding:4;"/>
					
							</td>
						</tr>
				</table>
			</div>
		</div>
		<div id="container-bottom">
			<input type="button" id="buttonok" value="[[OK]]" class="formbutton" style="width:70px"	onclick="do_insert();" /> 
			&nbsp;&nbsp;&nbsp;&nbsp; 
			<input type="button" id="buttoncancel" value="[[Cancel]]" class="formbutton" style="width:70px"	onclick="do_Close();" />	
		</div>
	</div>
	</body>
</html>

