using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Mail;
using MimeKit;
using System.Web;

namespace Orbium_Gmail_API
{
    public partial class SendGmail : System.Web.UI.Page
    {
        static string[] Scopes = {
            GmailService.Scope.GmailReadonly,
            GmailService.Scope.MailGoogleCom,
            GmailService.Scope.GmailSend,
        };
        static string ApplicationName = "orbapie-mail";



        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public static string Base64UrlDecode(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return "<strong>Message body was not returned from Google</strong>";

            string InputStr = input.Replace("-", "+").Replace("_", "/");
            return System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(InputStr));
        }
        public static string Base64UrlEncode(string input)
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            return Convert.ToBase64String(inputBytes).Replace("+", "-").Replace("/", "_").Replace("=", "");
        }
        public class EmailItem //get msgs
        {
            private Image _pic;
            public string From { get; set; }
            public string Subject { get; set; }
            public string Date { get; set; }
            public string Body { get; set; }
            public Image Pic
            {
                get { return _pic; }
                set { _pic = value; }
            }
        }
        public void EmailGet()
        {

            try
            {
                GmailService gs = Service(Credential());
                UsersResource.MessagesResource.ListRequest msg = gs.Users.Messages.List("me");

                msg.LabelIds = "INBOX"; //get just messages from INBOX
                msg.Q = "label:unread"; //get just unread messages
                msg.MaxResults = 10; //max number of messages to return
                msg.PrettyPrint = false;

                IList<Message> msgs = msg.Execute().Messages;

                if (msgs != null && msgs.Count > 0)
                {
                    var msglist = new List<EmailItem>();

                    foreach (var msgsItem in msgs)
                    {
                        Message m = gs.Users.Messages.Get("me", msgsItem.Id).Execute();

                        var from = (m.Payload.Headers.SingleOrDefault(h => h.Name == "From")?.Value);
                        var date = (m.Payload.Headers.SingleOrDefault(h => h.Name == "Date")?.Value);
                        var subject = (m.Payload.Headers.SingleOrDefault(h => h.Name == "Subject")?.Value);

                        //body
                        var body = "";
                        Image pic = null;

                        if (m.Payload.MimeType.Contains("multipart")) //the mime type identify if the body text has more than one part
                        {
                            StringBuilder sb = new StringBuilder();
                            Image picid = null;
                            for (int i = 0; i <= m.Payload.Parts.Count() - 1; i++)
                            {
                                if (m.Payload.Parts[i].MimeType.Contains("text"))
                                {
                                    string s = Base64UrlDecode(m.Payload.Parts[i].Body.Data);
                                    sb.Append(s);//get all body parts 
                                }
                                if (m.Payload.Parts[i].MimeType.Contains("multipart")) //Parts may contain attachments, so get the subparts
                                {
                                    for (int ia = 0; ia <= m.Payload.Parts[i].Parts.Count() - 1; ia++)
                                    {
                                        string s = Base64UrlDecode(m.Payload.Parts[i].Parts[ia].Body.Data);
                                        sb.Append(s);//get all body parts 
                                    }
                                }
                                if (m.Payload.Parts[i].Body.AttachmentId != null && m.Payload.Parts[i].MimeType.Contains("image")) //Attachment image
                                {
                                    string attachid = m.Payload.Parts[i].Body.AttachmentId;
                                    string a = gs.Users.Messages.Attachments.Get("me", msgsItem.Id, attachid).Execute().Data;
                                    byte[] bytes = Convert.FromBase64String(a.ToString().Replace("-", "+").Replace("_", "/"));

                                    Image image;
                                    using (MemoryStream ms = new MemoryStream(bytes))
                                    {
                                        image = Image.FromStream(ms);
                                    }
                                    picid = image;
                                }
                            }
                            pic = picid;
                            body = sb.ToString();
                        }
                        else
                        {
                            body = Base64UrlDecode(m.Payload.Body.Data);
                        }

                        msglist.Add(new EmailItem() { From = from, Subject = subject, Date = date, Body = body, Pic = pic });
                    }
                    GridView1.DataSource = msglist;
                    GridView1.DataBind();
                }
                gs.Dispose();
            }
            catch (Exception ex)
            {
                Label1.Text = "No Receive. Error: " + ex;
            }
        }
        public void EmailGenandSend()
        {
            try
            {
                string email = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "email.html");
                string subject = "Subject Teste 4";
                string bodyhtml = "<h1>Body Test </h1>\r\n\r\n" + email;
                string[] attachments = { AppDomain.CurrentDomain.BaseDirectory + "\\" + "attachments\\" + "file1.txt",
                                         AppDomain.CurrentDomain.BaseDirectory + "\\attachments\\" + "file2.txt",
                                         AppDomain.CurrentDomain.BaseDirectory + "\\attachments\\" + "logo-orbium.jpg"
                                       };
                string from = "daniel.prado@orbium.com.br";
                string to = "dop.jr82@gmail.com";

                Sendmail(subject, bodyhtml, attachments, from, to);
            }
            catch (Exception ex)
            {
                Label1.Text = "ERRO: " + ex;
            }
        }
        protected void Sendmail(string subject, string bodyhtml, string[] attachments, string from, string to)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.Subject = subject;
                mail.Body = bodyhtml;
                mail.From = new MailAddress(from);
                mail.IsBodyHtml = true;

                foreach (string add in from.Split(','))
                {
                    if (string.IsNullOrEmpty(add))
                        continue;

                    mail.To.Add(new MailAddress(add));
                }

                foreach (string add in to.Split(','))
                {
                    if (string.IsNullOrEmpty(add))
                        continue;

                    mail.CC.Add(new MailAddress(add));
                }

                foreach (string path in attachments)
                {
                    string mimeTypes = MimeMapping.GetMimeMapping(path); //the type of attach "image/jpeg"

                    if (mimeTypes.Contains("image")) //to body images
                    {
                        LinkedResource res = new LinkedResource(path);
                        res.ContentId = Guid.NewGuid().ToString();
                        res.ContentType = new System.Net.Mime.ContentType(mimeTypes);
                        mail.Body = string.Format(bodyhtml, @"<img src='cid:" + res.ContentId + @"'/>");
                        AlternateView alternateView = AlternateView.CreateAlternateViewFromString(mail.Body, null, System.Net.Mime.MediaTypeNames.Text.Html);
                        alternateView.LinkedResources.Add(res);
                        
                        mail.AlternateViews.Add(alternateView);

                        //Attachment attachment = new Attachment(path);
                        //attachment.ContentId = attachment.ContentType.Name;
                        //attachment.ContentType = new System.Net.Mime.ContentType(mimeTypes);
                        //attachment.ContentDisposition.Inline = true;
                        //attachment.ContentDisposition.DispositionType = System.Net.Mime.DispositionTypeNames.Inline;
                        //mail.Attachments.Add(attachment);

                        //mail.Body = string.Format(bodyhtml, attachment.ContentId);
                    }
                    else
                    {
                        Attachment attachment = new Attachment(path);
                        mail.Attachments.Add(attachment);
                    }

                }

                MimeMessage mimeMessage = MimeMessage.CreateFromMailMessage(mail);

                Message message = new Message();
                message.Raw = Base64UrlEncode(mimeMessage.ToString());
                GmailService gs = Service(Credential());
                gs.Users.Messages.Send(message, "me").Execute();
            }
            catch (Exception ex)
            {
                Label1.Text = "ERROR: " + ex;
            }
        }
        public GmailService Service(UserCredential credential)
        {
            // Create Gmail API service.
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            return service;
        }
        public UserCredential Credential()
        {
            UserCredential credential;
            // Get credential 
            var json = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "credentials.json");

            string path = AppDomain.CurrentDomain.BaseDirectory + "credentials.json";
            using (var stream =
                new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes, //seted up
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Label1.Text = "Credential file saved to: " + credPath; //save the credential used to a path. (token.json to credPath)
            }

            return credential;
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            EmailGenandSend();
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            EmailGet();
        }
    }
}
